# Copy all assets from any app on the App Store

## Tools needed:
Apple Configurator 2 
### https://apps.apple.com/it/app/apple-configurator-2/id1037126344?mt=12
Asset Catalog Tinkerer
### https://github.com/insidegui/AssetCatalogTinkerer/raw/master/releases/AssetCatalogTinkerer_latest.zip

## Instructions
Open Apple Configurator 2 and connect your iPhone.

<img src="Images/image2.png" width="1000">

Press on the Add button and then App

<img src="Images/image3.png" width="250">

<img src="Images/image4.png" width="600">

Press "Add" and wait until this message appears

<img src="Images/image5.png" width="600">

Now, open Finder and look for “Go to Folder”.

<img src="Images/image6.png" width="500">


Copy and paste this string inside the prompt:
### ~/Library/Group Containers/ K36BKF7T3D.group.com.apple.configurator/ Library/Caches/Assets/TemporaryItems/ MobileApps

Go through the folder until you find an .ipa file.

<img src="Images/image7.png" width="150">

Copy the file onto Desktop and change the extension from “.ipa” in “.zip”. Finder will complain about the extension change. Press “Use .zip”

<img src="Images/image8.png" width="400">

Unzip the file.

Now open Asset Catalog Tinkerer (no window will appear, but on top of the screen, press on Open)


<img src="Images/image9.png" width="600">

<img src="Images/image10.png" width="600">

Search inside the unzipped folder until you find the file called “Assets.car”.

<img src="Images/image11.png" width="700">


All the assets will be there. You can drag and drop them to place wherever you want.
